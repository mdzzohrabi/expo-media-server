module.exports = {
    httpPort: 1350,
    supportedImages: ['.png', '.jpg', '.jpeg'],
    supportedVideos: ['.mp4', '.webm'],
    publicPath: '/media',
    cacheDir: __dirname + '/cache',
    assetUrl: 'http://api.beshenas.com'
}
