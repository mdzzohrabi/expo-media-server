/**
 * Media Optimzier Service
 * @author Masoud Zohrabi <mdzzohrabi@gmail.com>
 */

let { static } = require('express');
let app = require('express')();
let sharp = require('sharp');
let { default: ThumbnailGenerator } = require('video-thumbnail-generator');
let ffmpeg = require('fluent-ffmpeg');
let path = require('path');
let { promisify } = require('util');
let { exists, mkdir } = require('fs');
let request = require('request');
let { httpPort, supportedImages, supportedVideos , cacheDir, publicPath, assetUrl } = require('./config');
let [ existsAsync, mkdirAsync ] = [ promisify(exists), promisify(mkdir) ];

let [customHost, customPort] = process.argv[3] ? process.argv[3].split(':') : [ null, null ];
let publicDir = process.argv[2];
let converting = [];

if (!publicDir) {
    console.log('Please enter a directory');
}

// Static resources
app.use(publicPath, static(cacheDir));

/**
 * @typedef HandleReqOptions
 * @property {string} url Media url
 * @property {number=} width Resize to Width
 * @property {number=} height Resize to Height
 * @property {boolean=} thumb Return the thumbnail of video
 * @property {boolean=} hls Convert video to HLS
 * @property {string=} fit Image resize mode
 */

 /**
  * 
  * @param {Express.Request} req Request
  * @param {Express.Response} res Response
  * @param {Function} next Next
  * @param {HandleReqOptions} param3 
  */
async function handleRequest(req, res, next, { url, width, height, thumb = false, hls = false, fit = 'cover' }) {

    let isUrl = true;
    let imageUrl = url;

    if (!['contain', 'cover', 'fill', 'inside', 'outside'].includes(fit)) fit = 'cover';

    if (!imageUrl) return next(`Invalid request parameters`);

    let imagePublicUrl = imageUrl;

    // Extension
    if ( !imageUrl.startsWith('http://') && !imageUrl.startsWith('https://') ) {
        imagePublicUrl = assetUrl + imageUrl;
        imageUrl = publicDir + '/' + imageUrl.replace(/[.]{2,}|^\//g, '');
        isUrl = false;
    }
    let ext = path.extname(imageUrl).toLowerCase();

    // Supported Image
    if ( (width || height) && supportedImages.includes( ext ) ) {
        
        let cacheName = `${ Buffer.from(imageUrl).toString('base64') }-${width}-${height}-${fit}${ext}`;

        if ( await existsAsync(cacheDir + '/' + cacheName) ) return res.sendFile(cacheDir + '/' + cacheName);

        let resizeImage = function (err, _res, body) {
            if (err) return next(err);

            sharp(body)
                .resize(width, height, { fit: fit, background: '#ffffff' })
                .toFile(cacheDir + '/' + cacheName, (err, info) => {
    
                    if (err) return next(err);   
                    res.sendFile(cacheDir + '/' + cacheName);
    
                });
        };

        if (isUrl) {
            request(imageUrl, { encoding: null }, resizeImage);
        }
        else {
            if (!await existsAsync(imageUrl)) return res.sendStatus(404);
            resizeImage(null, null, imageUrl);
        }  
    

    // Video thumbnail
    } else if ( thumb && supportedVideos.includes(ext) ) {

        let thumbName = 'thumb-' + Buffer.from(imageUrl).toString('base64') + '.png';

        if (await existsAsync(cacheDir + '/' + thumbName)) return res.sendFile(cacheDir + '/' + thumbName);

        let thumbFiles = [];

        ffmpeg(imageUrl)
        .on('filenames', files => {
            thumbFiles = files;
        })
        .on('end', () => {
            if ( thumbFiles.length > 1 )
                res.sendFile(cacheDir + '/' + thumbFiles.pop());
            else
                res.sendStatus(500);
        })
        .screenshot({ 
            count: 1,
            size: '320x240',
            folder: cacheDir,
            filename: thumbName,
            timestamps: [ '20%' ]
        });

    } else if ( hls && supportedVideos.includes(ext) ) {

        if (converting.indexOf(imageUrl) > 0) return imagePublicUrl;

        var hlsName = Buffer.from(imageUrl).toString('base64') + '.m3u8';
        let path = cacheDir + '/' + hlsName;

        converting.push(imageUrl);

        let worker = ffmpeg(imageUrl)
            .audioCodec('libopus')
            .audioBitrate(96)
            .outputOptions([
                '-hls_time 10',
                '-hls_playlist_type vod',
                `-hls_segment_filename ${cacheDir}/${Buffer.from(imageUrl).toString('base64')}-%03d.ts`,
                '-hls_base_url ' + assetUrl
            ])
            .output(path)
            .on('progress', function(progress) {
                console.log('Processing: ' + progress.percent + '% done')
            })
            .on('end', function(err, stdout, stderr) {
                console.log('Finished processing!' /*, err, stdout, stderr*/)
                converting.splice( converting.indexOf(imageUrl), 1);
            })
            .run()     

    } else {
        res.redirect(imagePublicUrl);
    }

}

app.get('/:mediaUrl/:width', function (req, res, next) {
    handleRequest(req, res, next, {
        url: req.params.mediaUrl,
        width: req.params.width
    })
})

app.get('/:mediaUrl', function (req, res, next) {
    handleRequest(req, res, next, {
        url: req.params.mediaUrl
    })
})


// End-Point
app.get('/', async function (req, res, next) {

    /** @type {string} */
    let imageUrl = req.query.u;
    let fit = req.query.fit;
    let width = Math.min( Number(req.query.w) , 2000 ) || undefined;
    let height = Math.min( Number(req.query.h) , 2000 ) || undefined;
    let thumb = !!req.query.thumb;
    let hls = !!req.query.hls;

    handleRequest(req, res, next, {
        url: imageUrl,
        width,
        height,
        thumb,
        hls,
        fit
    });    

});

async function startServer() {
    if ( !await existsAsync(cacheDir) )
        await mkdirAsync(cacheDir);

    let port = Number(customPort) || httpPort;
    let host = customHost || '0.0.0.0';

    // Start service
    app.listen(port, host, () => {
        console.log(`Media service started on ${ host }:${ port }`);
    });    
}

startServer();
